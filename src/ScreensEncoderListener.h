#ifndef SCREENSENCODERLISTENER_H
#define SCREENSENCODERLISTENER_H

#include <DelegateEncoderListener.h>
#include <ScreensStack.h>
#include "AppScreen.h"

class ScreensEncoderListener: public DelegateEncoderListener {
	
private:
	ScreensStack<AppScreen>& screensStack;
	
protected:

	virtual EncoderListener* getDelegateListener() override;
	
public:

	ScreensEncoderListener(
		ScreensStack<AppScreen>& screensStack_
	);

};


#endif //SCREENSENCODERLISTENER_H

