#ifndef TESTSCREEN_H
#define TESTSCREEN_H

#include "AppScreen.h"
#include <FontPaint.h>
#include <Screen.h>
#include <ScreensRouter.h>
#include <EncoderListener.h>
#include <Logger.h>
#include <TaggedLogger.h>

class TestScreen: public AppScreen {
	
private:

	Logger& simpleLogger;
	TaggedLogger logger;

	Font* font;
	FontPaint fontPaint;
	size_t number;
	size_t titleLength;
	char title[16];
	ScreensRouter<AppScreen>& screensRouter;
	
	TestScreen* nextScreen;
	
	void initializeTitle() {
		size_t number = this->number;
		titleLength = 0;
		if (number <= 0) {
			title[0] = '0';
			title[1] = 0;
			titleLength = 1;
			return;
		}
		while (number > 0) {
			title[titleLength] = (number % 10) + '0';
			number /= 10;
			titleLength++;
		}
		title[titleLength] = 0;
		for (size_t i = 0; i < titleLength/2; i++) {
			char temp = title[i];
			title[i] = title[titleLength-1-i];
			title[titleLength-1-i] = temp;
		}
	}
	
public:

	static const size_t maxNumber = 20;

	TestScreen(
		size_t number,
		ScreensRouter<AppScreen>& screensRouter_,
		Logger& logger_
	): screensRouter(screensRouter_), fontPaint(), logger(logger_, FCP("TestScreen")), simpleLogger(logger_) {
		this->number = number;
		fontPaint.setForeground(RGBAs::white);
		font = new ScaledFont(Font5x7::getInstance(), 6);
		fontPaint.setFont(*font);
		initializeTitle();
		if (number <= maxNumber) {
			nextScreen = new TestScreen(number+1, screensRouter, simpleLogger);
		} else {
			nextScreen = nullptr;
		}
	}

	virtual void draw(Canvas& canvas) override {
		size_t glyphsSeparation = 1;
		size_t cellWidth = font->getGlyphWidth() + glyphsSeparation;
		size_t titleWidth = cellWidth * titleLength - glyphsSeparation;
		size_t left = (canvas.getWidth() - titleWidth) / 2;
		size_t top = (canvas.getHeight() - font->getGlyphHeight()) / 2;
		for (size_t i = 0; i < titleLength; i++) {
			fontPaint.drawChar(title[i], canvas, left + cellWidth * i, top);
		}
		#ifdef DEBUG
			logger.log([&](Logger& log) {
			   log.log(FCP("Draw. titleLength="));
			   log.log(titleLength);
			   log.log(FCP(", title='"));
			   log.log(CP(title));
			   log.log(FCP("', cellWidth="));
			   log.log(cellWidth);
			   log.log(FCP(", titleWidth="));
			   log.log(titleWidth);
			   log.log(FCP(", left="));
			   log.log(left);
			   log.log(FCP(", top="));
			   log.log(top);
			});
		#endif
	}

	virtual void onEncoderClick() {
	}
	
	virtual void onEncoderOffset(int8_t offset) {
		if (offset > 0) {
			if (nextScreen != nullptr){
				screensRouter.forward(nextScreen);
			}
		} else {
			screensRouter.back();
		}
	}
	
};

#endif //TESTSCREEN_H
