#include "ScreensEncoderListener.h"

ScreensEncoderListener::ScreensEncoderListener(
		ScreensStack<AppScreen>& screensStack_
): screensStack(screensStack_) {
}

EncoderListener* ScreensEncoderListener::getDelegateListener() {
	return &(screensStack.getCurrentScreen());
}
