#ifndef APPSCREEN_H
#define APPSCREEN_H

#include <FontPaint.h>
#include <Screen.h>
#include <ScreensRouter.h>
#include <EncoderListener.h>

class AppScreen: public Screen, public EncoderListener {};

#endif //APPSCREEN_H
