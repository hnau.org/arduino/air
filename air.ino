//#define DEBUG

#include <Logger.h>
#include <Wire.h>
#include <SerialLogger.h>
#include <TaggedLogger.h>
#include <Thread.h>
#include <Threads.h>
#include <SSD1306.h>
#include <MHZ19.h>
#include <Font5x7.h>
#include <ScaledFont.h>
#include <CharsProvider.h>
#include <FontPaint.h>
#include <PrimitivesPaint.h>
#include <Encoder.h>
#include <SimpleEncoderListener.h>
#include "src/TestScreen.h"
#include "src/ScreensEncoderListener.h"

SerialLogger simpleLogger(9600);

const PROGMEM char airString[] = "Air";
TaggedLogger logger(simpleLogger, FPCP(airString));

SSD1306 ssd1306(0x3C, 128, 64);
PrimitivesPaint clearScreenPaint;


ScreensStack<AppScreen> screensStack(16, simpleLogger);
ScreensEncoderListener screensEncoderListener(screensStack);

const PROGMEM char ppmString[] = "PPM: ";
Thread* threads[] = {
        new MHZ19(
          2, 13, 
          9600, 
          []() -> timestamp { return 5000; },
          [&](uint16_t ppm) { 
            logger.log([&](Logger& log) { 
              log.log(FPCP(ppmString));
              log.log(ppm);
            });
          },
          simpleLogger
        ),
        new Encoder(
			12, 14, 16,
			screensEncoderListener
        )
};

Threads threadsManager(
        threads,
        sizeof(threads) / sizeof(Thread*)
);

std::function<void()> needRefreshListener = [&]() {
	#ifdef DEBUG
		logger.log([&](Logger& log){log.log(FCP("Draw"));});
	#endif
	clearScreenPaint.fill(ssd1306);
	screensStack.draw(ssd1306);
	ssd1306.commit();
};

void setup() {
	Wire.begin();
	
	clearScreenPaint.setColor(RGBAs::black);
	
	screensStack.forward(new TestScreen(0, screensStack, simpleLogger));
	screensStack.setNeedRefreshListener(&needRefreshListener);
}

void loop() 
{
	#ifdef DEBUG
		timestamp start = millis();
	#endif
	
    threadsManager.loop();
   
	#ifdef DEBUG
		logger.log([&](Logger& log) {
		   log.log(FCP("Loop: "));
		   log.log(millis() - start);
		   log.log(FCP("ms"));
		});
	#endif
}
